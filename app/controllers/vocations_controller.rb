class VocationsController < ApplicationController
  before_action :set_vocation, only: [:show]

  def show
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vocation
      @vocation = Vocation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vocation_params
      params.fetch(:vocation, {})
    end
end
