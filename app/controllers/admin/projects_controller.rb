class Admin::ProjectsController < Admin::ApplicationController
  def index
    @projects = Project.all
  end
  def new
    @project = Project.new
  end
  def edit
  @project = Project.find(params[:id])
  end
  def create
    @project = Project.new(model_params)
    if @project.save
      redirect_to admin_projects_path
    else
      render :new
    end
  end


  def show
    @project = Project.last
  end
  def update
    @project = Project.find(params[:id])
    if @project.update(model_params)
    redirect_to admin_project_path
    else
    render 'edit'
    end
  end
  def destroy
  @protect = Project.find(params[:id])
  @protect.destroy
  redirect_to admin_projects_path
  end
  private
  def model_params
    params.require(:project).permit(:title, :text, :photo)
  end
end
