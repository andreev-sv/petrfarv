class Admin::ApplicationController < ApplicationController

  before_action :authenticate

  protected

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "mainadmin" && password == "PfPass16"
    end
  end
end
