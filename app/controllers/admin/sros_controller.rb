class Admin::SrosController < Admin::ApplicationController
  def index
    @sros = Sro.all
  end
  def new
    @sro = Sro.new
  end
  def edit
  @sro = Sro.find(params[:id])
  end
  def create
    @sro = Sro.new(sro_params)
    if @sro.save
      redirect_to admin_sros_path
    else
      render :new
    end
  end
  def show
    @sro = Sro.find(params[:id])
  end
  def update
    @sro = Sro.find(params[:id])
    if @sro.update(sro_params)
    redirect_to admin_sro_path
    else
    render 'edit'
    end
  end
  def destroy
  @sro = Sro.find(params[:id])
  @sro.destroy
  redirect_to admin_sros_path
  end
  private
  def sro_params
    params.require(:sro).permit(:text, :photo)
  end
end
