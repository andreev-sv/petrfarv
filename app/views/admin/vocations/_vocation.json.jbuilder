json.extract! vocation, :id, :created_at, :updated_at
json.url vocation_url(vocation, format: :json)
