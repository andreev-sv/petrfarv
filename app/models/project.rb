class Project < ApplicationRecord
  include Paperclip::Glue

  has_attached_file :photo, styles: { big: "1200x500>", medium: "1000x657>", small: "500x300>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/

  validates :photo, attachment_presence: true
  validates :title, :text, presence: true
end
