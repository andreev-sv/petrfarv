require 'test_helper'

class VocationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vocation = vocations(:one)
  end

  test "should get index" do
    get vocations_url
    assert_response :success
  end

  test "should get new" do
    get new_vocation_url
    assert_response :success
  end

  test "should create vocation" do
    assert_difference('Vocation.count') do
      post vocations_url, params: { vocation: {  } }
    end

    assert_redirected_to vocation_url(Vocation.last)
  end

  test "should show vocation" do
    get vocation_url(@vocation)
    assert_response :success
  end

  test "should get edit" do
    get edit_vocation_url(@vocation)
    assert_response :success
  end

  test "should update vocation" do
    patch vocation_url(@vocation), params: { vocation: {  } }
    assert_redirected_to vocation_url(@vocation)
  end

  test "should destroy vocation" do
    assert_difference('Vocation.count', -1) do
      delete vocation_url(@vocation)
    end

    assert_redirected_to vocations_url
  end
end
