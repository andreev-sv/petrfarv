class CreateVocations < ActiveRecord::Migration[5.1]
  def change
    create_table :vocations do |t|
      t.string :title
      t.text :text
      t.timestamps
    end
  end
end
