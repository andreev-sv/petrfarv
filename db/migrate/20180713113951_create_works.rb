class CreateWorks < ActiveRecord::Migration[5.1]
  def change
    create_table :works do |t|
      t.string :title
      t.text :text
      t.string :object
      t.timestamps
    end
  end
end
