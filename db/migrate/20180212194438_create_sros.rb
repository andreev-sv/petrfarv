class CreateSros < ActiveRecord::Migration[5.1]
  def change
    create_table :sros do |t|
      t.text :text
      t.attachment :photo
      t.timestamps
    end
  end
end
