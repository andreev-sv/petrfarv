Rails.application.routes.draw do
  resources :vocations
  resources :works
  resources :supplies
  resources :users
  root 'static_pages#home'
  get '/home' => 'static_pages#home'
  get '/building' => 'static_pages#building'
  get '/projects' => 'static_pages#projects'
  get '/geo' => 'static_pages#geo'
  get '/about' => 'static_pages#about'
  get '/contacts' => 'static_pages#contacts'
  get '/hh' => 'static_pages#hh'
  get '/work' => 'static_pages#work'
  get '/supply' => 'static_pages#supply'
  resources :projects
  resources :sros

  namespace :admin do
    resources :projects
    resources :sros
    resources :vocations
    resources :works
    resources :supplies
    resources :users
    root 'projects#index'
  end
end
